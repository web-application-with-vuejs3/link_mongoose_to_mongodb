const mongoose = require('mongoose')
const Room = require('./models/Room')
const Building = require('./models/Building')
mongoose.connect('mongodb://localhost:27017/example')

async function main () {
  // Update
  // const room = await Room.findById('621a1c2e1126c355de58b3b5')
  // room.capacity = 20
  // room.save()
  // console.log(room)

  const room = await Room.findOne({ capacity: { $gte: 100 } }).populate('building')
  console.log(room)
  console.log('=============================')
  const rooms = await Room.find({ capacity: { $gte: 100 } }).populate('building')
  console.log(rooms)
  const buildings = await Building.find({}).populate('rooms')
  console.log(JSON.stringify(buildings))
}

main().then(function () {
  console.log('Finish')
})
